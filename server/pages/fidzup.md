# Fidzup 
[Site web](www.fidzup.com)

## Services
  * Real-TIme Bidding
  * Tracking GPS
  * Analyse des habitudes
  * Drive to Store
  * Retargeting
  * Tracking in-store avec un ptit boitier "FidBox" (Tracking via le son et le wifi)

Le "tracking via le son et le wifi" font l'objet de 2 brevets, présentés comme suit : "Fidzup a développé plusieurs technologies fonctionnant par le son (2 brevets déposés) ou par le WiFi qui permettent de détecter 40% des visiteurs grâce à leur mobile. Les données anonymes ainsi récupérées sont sécurisées et stockées et permettent de définir un profil consommateur/voyageur dont le comportement est finement analysé."

## Clients
  * Gémo
  * Timberland
  * Buffalo Grill
  * GrandOptical
  * Opel
  * Klepiierre
  * Etam
  * SNCF [Article de SNCF](http://www.sncf.com/fr/presse/article/incubateur-startup-fidzup)
  * JCDecaux (2015 mais toujours intéressant) : [Article Numerama](http://www.numerama.com/magazine/34310-la-cnil-s-oppose-au-tracage-des-pietons-par-wi-fi-a-la-defense.html)

## Brevets
Via le son : [EP2849179A1](http://bases-brevets.inpi.fr/fr/document/EP2849179.html?s=1503741745074&p=5&cHash=d2d5b9ae5809c0a097cd0ee2da3b1f5a)       
Localisation d'un terminal : [EP2747462](http://bases-brevets.inpi.fr/fr/document/EP2747462.html?s=1503741745074&p=5&cHash=d48214b57c37d039dcbb4e928d52fa1a)